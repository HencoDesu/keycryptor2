using System.Linq;
using System.Text;
using KeyCryptor2.Core.Keys;

namespace KeyCryptor2.Core.Encrypting {
	/// <summary>
	/// Compability with old (1.5) version
	/// </summary>
	public class LegacyCryptor : ICryptor {
		public string Encrypt(string value, Key key) {
			long offset = GetKeyOffset(key);
			var builder = new StringBuilder();

			foreach (char c in value) {
				builder.Append((char) (c + offset));
			}

			return builder.ToString();
		}

		public string Decrypt(string value, Key key) {
			long offset = GetKeyOffset(key);
			var builder = new StringBuilder();

			foreach (char c in value) {
				builder.Append((char) (c + offset));
			}

			return builder.ToString();
		}

		private long GetKeyOffset(Key key) {
			int index = 0;

			return key.Value.Aggregate<char, long>(0, (current, symbol) => current + symbol * index++ / 4);
		}
	}
}