using KeyCryptor2.Core.Keys;

namespace KeyCryptor2.Core.Encrypting {
	/// <summary>
	/// Interface that provides a crypting and encrypting methods using key
	/// </summary>
	public interface ICryptor {
		/// <summary>
		/// Encrypt a string
		/// </summary>
		/// <param name="value">String to encrypt</param>
		/// <param name="key">Key used for encrypting</param>
		/// <returns>Encrypted string</returns>
		string Encrypt(string value, Key key);
		
		/// <summary>
		/// Decrypt a string
		/// </summary>
		/// <param name="value">String to decrypt</param>
		/// <param name="key">Key used for decrypting</param>
		/// <returns>Decrypted string</returns>
		string Decrypt(string value, Key key);
	}
}