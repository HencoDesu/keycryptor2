namespace KeyCryptor2.Core.Keys {
	/// <summary>
	/// Key wrapper
	/// </summary>
	public struct Key {
		/// <summary>
		/// This key string value
		/// </summary>
		public string Value { get; }

		/// <summary>
		/// Initializes new instance of <seealso cref="Key"/>
		/// </summary>
		/// <param name="value"></param>
		public Key(string value) {
			Value = value;
		}
	}
}