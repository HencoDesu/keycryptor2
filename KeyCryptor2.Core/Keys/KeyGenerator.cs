using System;
using System.Text;

namespace KeyCryptor2.Core.Keys {
	/// <summary>
	/// Generates new <seealso cref="Key"/>
	/// </summary>
	public class KeyGenerator {
		private Random rnd = new Random();

		/// <summary>
		/// Generated key min length
		/// </summary>
		public int MinLength { get; set; }
		
		/// <summary>
		/// Generated key max length
		/// </summary>
		public int MaxLength { get; set; }
		
		public Key GenerateNew() {
			int length = rnd.Next(MinLength, MaxLength + 1);
			var builder = new StringBuilder();

			for (int position = 0; position < length; position++) {
				builder.Append((char) rnd.Next(41, 177));
			}
			
			return new Key(builder.ToString());
		}
	}
}